#pragma once
class Integral
{
protected:
	float m_a, m_b;
	float(*m_func)(float);
public:
	Integral(float a, float b, float(*func)(float));
	~Integral();
	virtual float Calculate(float dx = 0.1f) = 0;
};

