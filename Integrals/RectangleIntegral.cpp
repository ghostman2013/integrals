#include "RectangleIntegral.h"



RectangleIntegral::RectangleIntegral(float a, float b, float(*func)(float))
	: Integral(a, b, func)
{
}


RectangleIntegral::~RectangleIntegral()
{
}

float RectangleIntegral::Calculate(float e)
{
	float res = 0;

	for (float x = m_a; x < (m_b - e); x += e) {
		res += m_func(x) * e;
	}

	return res;
}
