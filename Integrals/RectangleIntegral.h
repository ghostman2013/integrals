#pragma once

#include "Integral.h"

class RectangleIntegral : public Integral
{
public:
	RectangleIntegral(float a, float b, float(*func)(float));
	~RectangleIntegral();
	float Calculate(float dx = 0.1f) override;
};

