#include "SimpsonIntegral.h"



SimpsonIntegral::SimpsonIntegral(float a, float b, float(*func)(float))
	: Integral(a, b, func)
{
}


SimpsonIntegral::~SimpsonIntegral()
{
}

float SimpsonIntegral::Calculate(float dx)
{
	float res = 0;
	float old_x = m_a;

	for (float x = m_a + dx; x < m_b; x += dx) {
		float fx0 = m_func(x);
		float fx1 = m_func((old_x + x) * 0.5f);
		float fx2 = m_func(old_x);
		res += dx * (fx0 + 4 * fx1 + fx2) / 6.0f;
		old_x = x;
	}

	return res;
}
