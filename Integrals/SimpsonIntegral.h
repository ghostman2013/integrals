#pragma once
#include "Integral.h"
class SimpsonIntegral : public Integral
{
public:
	SimpsonIntegral(float a, float b, float(*func)(float));
	~SimpsonIntegral();
	float Calculate(float dx = 0.1f) override;
};

