#include "TrapezeIntegral.h"



TrapezeIntegral::TrapezeIntegral(float a, float b, float(*func)(float))
	: Integral(a, b, func)
{
}


TrapezeIntegral::~TrapezeIntegral()
{
}

float TrapezeIntegral::Calculate(float dx)
{
	float res = 0;
	float old_y = m_func(m_a);

	for (float x = m_a + dx; x < m_b; x += dx) {
		float y = m_func(x);
		res += dx * (y + old_y) / 2.0f;
		old_y = y;
	}

	return res;
}
