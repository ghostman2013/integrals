#pragma once

#include "Integral.h"

class TrapezeIntegral : public Integral
{
public:
	TrapezeIntegral(float a, float b, float(*func)(float));
	~TrapezeIntegral();
	float Calculate(float dx = 0.1f) override;
};

