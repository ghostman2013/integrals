#include "pch.h"
#include "integrals.h";

using namespace std;

float get_float(const string &);
float x2(float);
float kxb(float);
float y(float);

void main() {
	float a = get_float("Input a low integral limit:");
	float b = get_float("Input a high integral limit:");
	auto integral = new SimpsonIntegral(a, b, &x2);
	cout << integral->Calculate(0.1f) << endl;

	system("pause"); // Wait for any key
}

float get_float(const string & msg) {
	float f;
	cout << msg << endl;
	cin >> f;
	return f;
}

float x2(float x) {
	return x*x;
}

float kxb(float x) {
	return 3 * x + 5;
}

float y(float x) {
	return 5;
}